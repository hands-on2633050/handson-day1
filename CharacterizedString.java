import java.util.Scanner;

public class CharacterizedString {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ketik kata/kalimat, lalu tekan Enter: ");
        String word = scanner.nextLine();
        printDivider(20);
        System.out.println(word);
        printDivider(20);
        
        System.out.println("Process memecahkan kata/kalimat menjadi karakter...");
        System.out.println(".".repeat(10));
        printDivider(20);

        for(int i = 0; i < word.length(); i++){
            System.out.printf("Karakter[%d]: %s\n", i, word.charAt(i));
        }

        scanner.close();

    }

    static void printDivider(int amount){
        System.out.println("=".repeat(amount));
    }
}